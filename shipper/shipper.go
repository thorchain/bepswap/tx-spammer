package shipper

import (
	"fmt"
	"strings"

	"gitlab.com/thorchain/tx-spammer/types"
)

type shipType uint8

const (
	ShipUnknown shipType = iota
	ShipStdOut
	ShipBinance
	ShipStateChain
)

var stringToShipperMap = map[string]shipType{
	"unknown":    ShipUnknown,
	"stdout":     ShipStdOut,
	"binance":    ShipBinance,
	"statechain": ShipStateChain,
}

// converts a string into a txType
func StringToShipper(s string) (shipType, error) {
	sl := strings.ToLower(s)
	if t, ok := stringToShipperMap[sl]; ok {
		return t, nil
	}
	return ShipUnknown, fmt.Errorf("Invalid ship type: %s", s)
}

type Shipper interface {
	Send(tx types.Transaction) error
}
