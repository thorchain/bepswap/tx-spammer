[![pipeline status](https://gitlab.com/thorchain/bepswap/tx-spammer/badges/master/pipeline.svg)](https://gitlab.com/thorchain/bepswap/tx-spammer/commits/master)
[![coverage report](https://gitlab.com/thorchain/bepswap/tx-spammer/badges/master/coverage.svg)](https://gitlab.com/thorchain/bepswap/tx-spammer/commits/master)

TX-Spammer
==========
A tool for generating tx on binance chain to create transactions like swaps,
stakes, and unstakes.

There is also a mode where it acts a mock observer, sending data into a
statchain node.

## Install
To install run 
```bash
make install
tx-spammer --help
```
It will be installed in your `$GOBIN` directory. Make sure it in in your
`$PATH`.


## Deverlopment

### Testing
```bash
make test
```

You can watch the filesystem for changes and run tests too.
```bash
make test-watch
```

### Linting
```bash
make lint
```
