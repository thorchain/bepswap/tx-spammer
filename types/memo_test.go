package types

import (
	"gitlab.com/thorchain/bepswap/common"
	. "gopkg.in/check.v1"
)

type MemoSuite struct{}

var _ = Suite(&MemoSuite{})

func (s MemoSuite) TestMemo(c *C) {
	ticker, err := common.NewTicker("BNB")
	c.Assert(err, IsNil)

	amt, err := common.NewAmount("34.349")
	c.Assert(err, IsNil)

	create := NewCreatePoolMemo(ticker)
	c.Check(create.String(), Equals, "CREATE:BNB")

	stake := NewStakeMemo(ticker)
	c.Check(stake.String(), Equals, "STAKE:BNB")

	unstake := NewUnStakeMemo(ticker, amt)
	c.Check(unstake.String(), Equals, "WITHDRAW:BNB:34.349")
}
